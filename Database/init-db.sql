--Create custom user_type type
DROP TYPE IF EXISTS user_type;
CREATE TYPE user_type AS ENUM ('USER', 'ADMIN');

DROP TYPE IF EXISTS post_type;
CREATE TYPE post_type AS ENUM ('OFFER', 'REQUEST');

DROP TYPE IF EXISTS post_category;
CREATE TYPE post_category AS ENUM ('MATERIALS', 'SHELTER', 'FOOD');

CREATE TABLE IF NOT EXISTS users (
    user_id varchar PRIMARY KEY,
    username varchar,
    picture varchar,
    email varchar,
    userType user_type NOT NULL,
    is_verified bool NOT NULL,
    is_enabled bool NOT NULL
);

CREATE TABLE IF NOT EXISTS posts (
    post_id SERIAL PRIMARY KEY,
    user_id varchar NOT NULL,
    postType post_type NOT NULL,
    category post_category NOT NULL,
    title varchar NOT NULL,
    description varchar NOT NULL,
--     Todo VERIFICAT CUM ADAUG FISIERE IN BAZA
    image varchar,
    upvotes integer NOT NULL,
    upvoted bool NOT NULL,
    downvoted bool NOT NULL,
    interactionCount integer NOT NULL,
    isRetired bool NOT NULL,
    tags varchar[] NOT NULL,
    interactions integer[],
    CONSTRAINT fk_user
        FOREIGN KEY(user_id)
        REFERENCES users(user_id)
--     CONSTRAINT fk_interaction
--         FOREIGN KEY(interaction_id)
--         REFERENCES interactions(interaction_id)
);

CREATE TABLE IF NOT EXISTS interactions (
    -- foreign key catre posts table?
    interaction_id SERIAL PRIMARY KEY,
    post_id SERIAL NOT NULL,
    textProp varchar NOT NULL,
    phone varchar NOT NULL,
    email varchar NOT NULL,
    posterName varchar NOT NULL,
    CONSTRAINT fk_post
        FOREIGN KEY(post_id)
        REFERENCES posts(post_id)
);